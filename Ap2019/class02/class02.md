#### Class 02 | Week 7 | 12 Feb 2019: Geometric Shapes
##### With Wed tutorial session and Fri shutup and code session.

### Messy notes:

#### Agenda:
1. Multi by David Reinfurt
2. Multiple dimensions of emoji from graphic to cultural forms
3. Basic structure: HTML, CSS, JS
4. Sample code: Coordination, Variables, Geometry, Shapes and Color
5. Walkthrough next mini-ex2: Geometric emoji
---

### 1. Multi by David Reinfurt

<img src="https://rag532wr4du1nlsxu2nehjbv-wpengine.netdna-ssl.com/wp-content/uploads/2014/09/Reinfurt_BW-600x0-c-default.jpg" width ="300">

- independent graphic designer and writer in New York City
- more: https://arts.princeton.edu/people/profiles/reinfurt/

What's <a href="http://www.o-r-g.com/apps/multi">Multi</a>:

- Multi on a phone: <img src="http://o-r-g.com/media/00028.png" height="500">
- Multi on the web: http://www.data-browser.net/
- Multi on a book: http://www.data-browser.net/db06.html
- Present 1728 possible arrangements

Background:

- <img src="http://www.o-r-g.com/media/00031.png" width="300">
reference: La Mela (The Apple), drew by Italian's designer Enzo Mari (1932)
- reduced to essential **form**
- part of the <a href="http://www.deliciousindustries.com/blog/enzo-maris-nature-series">nature series</a>
- considered the process of "the serial logic of industrial reproduction" and reflect upon contemporary software production

### 2. Multiple dimensions of emoji from graphic to cultural forms

Discussion

1. What is your take away from reading the article?
2. Why does emoji are considered as technological systems that are intensely interact with diverse physical bodies? Where are the bodies and what constitute technological systems?  

### 3. Basic structure: HTML, CSS, JS
![Web](https://www.codeproject.com/KB/books/DevelopWebWidgetHtmlCssJs/bigpicture_small.png "web")

*Reference image: [Creating Web Widget with HTML, CSS, and JavaScript by Raj Lal](https://www.codeproject.com/Articles/81355/Chapter-Creating-Web-Widget-with-HTML-CSS-and-Ja)*

#### 4. Sample code
- [Run me](https://cdn.staticaly.com/gl/siusoon/aesthetic-programming/raw/master/Ap2019/class02/sketch/index.html) (source: [sketch02-sample code](https://gitlab.com/siusoon/aesthetic-programming/tree/master/Ap2019/class02/sketch))
- <img src="https://gitlab.com/siusoon/aesthetic-programming/raw/master/Ap2019/class02/sketch02_code.gif">

##### Coordinate system (https://p5js.org/examples/structure-coordinates.html)

<img src="https://processing.org/tutorials/drawing/imgs/drawing-03.svg" width="550">

*Reference image: [Processing](https://processing.org)*

  - Numbers and pixels
  - createCanvas(), width, height
```javascript
function setup() {
  createCanvas(500, 600);
}
```
try:
```javascript
console.log(width, height);
```
##### Variables
   - Naming: [Variables](https://p5js.org/examples/data-variables.html)
   - think in terms of a container, where a value is stored and it can be changed over time.
   - e.g text, number, URL, etc
   - Technically: it registers a memory cell/location (RAM) with the address, name, type and value
   - interesting questions: how to remember(store) and how to forget(erase)?
   (if you are interested, see [here: Memory: To remember and forget](http://poeticcomputation.info/chapters/ch.2/)
   ![memory2](https://thetechjournal.com/wp-content/uploads/2012/10/ram-windows-speed-up.jpg)
   ![memory](http://www.tenouk.com/clabworksheet/labworksheet6_files/cprogrammingscanf004.png)

```javascript
let moving_size = 60;
let static_size = 20;
.
.
.
ellipse(mouseX, mouseY, moving_size, moving_size);
```
##### basic arithmetic operators

  ```
   - add(+): Addition and concatenation > or both numbers and text/characters
   - subtract(-)
   - multiply(*)  
   - divide(/)
   - Special operators: increment (++), decrement (--)                  
  ```
  try
  ```javascript
  console.log("hello " + "world");
  console.log(2*8);
  ```

  - different data types (e.g text and numbers)

##### Geometry and Shapes
- A branch of mathematics
- concerning the properties of the configuration of 'points' (no dimension), 'lines' (one-dimension), 'planes' (two-dimension)
- This also relates to shapes, size, relative position, and the properties of space.

**Discussion:**
1. Try to look at the sample code and run it in your own computer
2. Explain the functions rect(), ellipse(), vertex(), beginShape(), stroke(), strokeWeight(), noFill(), fill(), mouseX, mouseY,  etc
3. What is frameRate?

##### colors
- color related syntax [here](https://p5js.org/reference/#group-Color)
- links [here](https://p5js.org/learn/color.html)
```javascript
fill(color(0,0,0));
rect(50,110,100,26); //left rectangle
.
.
.
noFill();
stroke(255,255,255);
strokeWeight(2);
```

#### Code challenge

- How would you draw a small inverted triangle as the nose?

---
#### 5. Walkthrough next mini-ex2 and next week
- See [here](https://gitlab.com/siusoon/aesthetic-programming/tree/master/Ap2019/all_mini_ex) for the mini ex2
- Peer-tutoring: Group 1 / Respondents: Group 2, Topic: push()/pop()
