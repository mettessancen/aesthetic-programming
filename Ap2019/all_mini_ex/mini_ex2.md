# Weekly mini ex2: due week 8, Monday night - Geometric emoji

**Objective:**
- To experiment various geometric drawing's possibilites, especially on shapes drawing with colors.
- To reflect critically on emoji via the provided text on "Modifying the Universal"

**Get some additional inspiration here:**
- [Multi](http://o-r-g.com/apps/multi) by David Reinfurt
- [Machine Feeling- call for participation](http://www.aprja.net/machine-feeling-call/)
- [Chinese Characters as Ancient Emoji](https://publish.illinois.edu/iaslibrary/2015/10/21/chinese-characters/)

**Tasks (RUNME and README):**
1. Make sure you have read/watch the required readings/instructional videos and references
2. Explore different shape related syntax
3. Design at least two emoji icons and reflect upon how does emoji relate to identity and cultural phenomena
4. Upload your 'runme' to your own Gitlab account under a folder called **mini_ex2**. (Make sure your program can be run on a web browser)
5. Create a readme file (README.md) and upload to the same mini_ex2 directory (see [this](https://www.markdownguide.org/cheat-sheet) for editing the README). The readme file should contain the followings:
  - A screenshot about your program (search for how to upload a screenshot image and link to your gitlab)
  - A URL link to your program and run on a browser, see: https://www.staticaly.com/
  - **Describe** your program and what you have used and learnt
  - **What** have you learnt from reading the assignment reading? **How** would you put your emoji into a wider cultural context that concerns identification, identity, race, social, economics, culture, device politics and beyond? (Try to think through the assigned reading and your coding process, and then expand that to your experience and thoughts)

6. Provide peer-feedback to 2 of your classmates on their works by creating "issues" on his/her gitlab corresponding repository. Write with the issue title "Feedback on mini_ex(?) by (YOUR FULL NAME)"  (Feedback is due before next Wed tutorial class while the readme and runme are due on next Mon)

NB1: Feel Free to explore and experiment more syntax and computational structures.

NB2: The readme file should be within 5600 characters.

**mini exercise peer-feedback: guideline**
1. (Just think, no need to write) Think about what kind of feedback you think would be useful to others. What kind of feedback you want to receive by yourself?
2. Describe (basic) what is the program about, what syntaxes were used, what does the work express?
3. Do you like the design of the program, and why? and which aspect do you like the most? How would you interprete the work?
2. How about the conceptual linkage about the work beyond technical description and implementation? What's your thoughts on this?
